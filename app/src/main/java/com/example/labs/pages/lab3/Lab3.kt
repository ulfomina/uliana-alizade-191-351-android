package com.example.labs.pages.lab3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.labs.R
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class Lab3 : Fragment() {
    private lateinit var lab3ViewModel: Lab3ViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        lab3ViewModel = ViewModelProvider(this).get(Lab3ViewModel::class.java)

        val root = inflater.inflate(R.layout.lab3, container, false)
        val number: TextView = root.findViewById(R.id.number)
        val responseView: TextView = root.findViewById(R.id.response)
        val downloadBtn: Button = root.findViewById(R.id.download_button)

        val scope = CoroutineScope(Job())
        var url =
            "https://yandex.ru/pogoda/moscow?utm_source=serp&utm_campaign=wizard&utm_medium=desktop&utm_content=wizard_desktop_main&utm_term=title&lat=55.753215&lon=37.622504"
//        var response: String
        downloadBtn.setOnClickListener {
            val job = scope.async {
                fetchWebsiteContents(url)
            }
            scope.launch(Dispatchers.Main){
                val doc = job.await()
                responseView.text = doc.text()
                val price = doc.select("div.temp.fact__temp.fact__temp_size_s > span.temp__value.temp__value_with-unit")
                number.text = price.text()
            }

        }

        return root
    }

     private  fun fetchWebsiteContents(url: String): Document {
        return Jsoup.connect(url).get()
    }


}
